FROM ipython/notebook

ENV HOME /root
ENV PATH $HOME/.rbenv/bin:$HOME/.rbenv/shims:$PATH
ENV RUBY_VERSION 2.2.1
ENV SHELL /bin/bash
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

#install build deps
RUN apt-get update && apt-get install -y build-essential libtool autoconf && apt-get clean

RUN apt-get install -y curl

#install rbenv ruby
RUN curl -L https://github.com/sstephenson/rbenv/archive/master.tar.gz \
  | tar zxf - \
  && mv rbenv-master $HOME/.rbenv
RUN curl -L https://github.com/sstephenson/ruby-build/archive/master.tar.gz \
  | tar zxf - \
  && mkdir -p $HOME/.rbenv/plugins \
  && mv ruby-build-master $HOME/.rbenv/plugins/ruby-build

RUN echo 'eval "$(rbenv init -)"' >> $HOME/.profile
RUN echo 'eval "$(rbenv init -)"' >> $HOME/.bashrc
RUN touch $HOME/.gemrc
RUN echo -e 'gem: --no-ri --no-rdoc\n' >> $HOME/.gemrc

# install rubinius
RUN apt-get install -y autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev \
	&& apt-get clean
RUN rbenv install $RUBY_VERSION
RUN rbenv global $RUBY_VERSION

RUN gem install iruby -v 0.2.6

# install usefull gems
RUN apt-get install -y libpq-dev && apt-get clean
RUN gem install pg && gem install  activerecord --version 4.1.10 && gem install activerecord-postgis-adapter

RUN gem install nyaplot

RUN mkdir -p /root/.ipython/kernels/ruby
COPY ./kernel.json /root/.ipython/kernels/ruby/kernel.json
